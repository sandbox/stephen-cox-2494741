Display a KML file on a map using the Leaflet and KML Leaflet plugin JavaScript
libraries, part of the Leaflet Plugins collection.

This module works independently to the Drupal Leaflet and Leaflet Plugin modules
although it is reliant on the two JavaScript Libraries


Installation
------------

 1. Download and enable the Libraries module.
 2. Download the Leaflet JavaScript library and unzip into a folder call leaflet
    in the site's Libraries directory; http://leafletjs.com/download.html
 3. Download and Leaflet Plugins JavaScript library and unzip into a folder
    called leaflet-plugins in the site's Libraries directory;
    https://github.com/shramov/leaflet-plugins
 4. Download and enable the kml_file module.

If everything was successful then there won't be any Leaflet related errors
shown on the site's status page.


Usage
-----

 1. Add a new File field to a content type with the KML File widget.
 2. Go to the content type's manage display page and change the format of the
    new file field to Rendered KML File.
 3. Click the gear icon to change the map and other settings.
 4. Create a new node of the content type and upload a KML file in the field
    previously created.
 5. The node should now display a map with the KML overlay.
